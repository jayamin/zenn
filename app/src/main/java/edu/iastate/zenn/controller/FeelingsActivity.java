package edu.iastate.zenn.controller;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.MenuItem;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;

import androidx.appcompat.widget.AppCompatImageView;
import androidx.core.app.NavUtils;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceManager;

import edu.iastate.zenn.R;
import edu.iastate.zenn.model.FeelingsActivityViewModel;

/**
 * Feelings activity where the user does their ZennSession™
 *
 * This activity is the CONTROLLER that interfaces with the data in FeelingsActivityViewModel
 * and updates the VIEW in activity_feelings.xml accordingly. It is where the user can see
 * their feeling responses or custom data. This is the ZennSession™.
 *
 * @author Jay Amin
 * @version 0.01
 */
public class FeelingsActivity extends AppCompatActivity {
    /**
     * Tag for logging
     */
    public String TAG = "CustomLog_FeelingsActivity";

    /**
     * ViewModel
     */
    private FeelingsActivityViewModel fvm;

    /**
     * TextView for feeling description
     */
    private TextView feelingDescriptionTV;

    /**
     * TextView for timer
     */
    private TextView timerTV;

    /**
     * ImageView for feeling emoji
     */
    public AppCompatImageView feelingIV;

    /**
     * SharedPreferences for persisting data
     */
    public SharedPreferences sharedPreferences;

    /**
     * Boolean for auto hide
     */
    private static final boolean AUTO_HIDE = true;

    /**
     * How many milliseconds for auto hide delay
     */
    private static final int AUTO_HIDE_DELAY_MILLIS = 3000;

    /**
     * Milliseconds for animation delay
     */
    private static final int UI_ANIMATION_DELAY = 300;

    /**
     * Boolean for visibility status
     */
    private boolean mVisible;

    /**
     * View with controls for view switches
     */
    private View mControlsView;

    /**
     * Handler for the hide methods
     */
    private final Handler mHideHandler = new Handler();

    /**
     * This is the followup runnable for the hide methods
     *
     */
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            feelingDescriptionTV.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };

    /**
     * This is the followup runnable for the show methods
     *
     */
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            ActionBar actionBar = getSupportActionBar();
            if (actionBar != null) {
                actionBar.show();
            }
            mControlsView.setVisibility(View.VISIBLE);
        }
    };

    /**
     * This is the runnable for the hide method
     *
     */
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    /**
     * This is the ontouch listener for the delayed hide
     *
     */
    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    /**
     * This is the observer using mutable livedata in the ViewModel for the timer
     *
     */
    private final Observer<Long> elapsedTimeObserver = new Observer<Long>() {
        @Override
        public void onChanged(@Nullable final Long newValue) {
            /*
            This block of code uses the mutable live data in the
            ViewModel and observes it to create a pseudo stopwatch.
             */
            if(newValue!= null && newValue<420){
                if(newValue>60){
                    unsubscribeTimer();
                }else{
                    displayTimerValue(60-newValue + "");
                    if(newValue==57){
                        fadeOutTV();
                    }
                }
            }
        }
    };

    /**
     * Method that runs every time FeelingsActivity is created
     *
     * @param savedInstanceState is the input bundle for the saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feelings);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        /*
        Init variables and components
         */
        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        feelingDescriptionTV = findViewById(R.id.feelings_text_tv);
        feelingDescriptionTV.setAlpha(0.0f);
        timerTV = findViewById(R.id.feelings_timer_tv);
        feelingIV = findViewById(R.id.feeling_iv);

        /*
        Associates the ViewModel and initializes the current feeling
         */
        fvm = ViewModelProviders.of(this).get(FeelingsActivityViewModel.class);
        String passedString = getIntent().getStringExtra("CURRENT_FEELING");
        fvm.setCurrentFeeling(passedString);

        /*
        This block of code uses a local string to set the image based on the passed uri
         */
        String tempStringUri = getIntent().getStringExtra("CUSTOM_IMG_URI");
        Log.d(TAG, "String extra URI passed was: " + tempStringUri);
        try{
            ;
            feelingIV.setImageURI(Uri.parse(tempStringUri));
        }catch (Exception e){
            Log.d(TAG,"could not set imageview or parse string into URI");
        }

        /*
        This block sets the custom text if it exists.
         */
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
        String preferencesCustomText = sharedPreferences.getString("text", "");
        if(preferencesCustomText.equals("")){
            Log.d(TAG, "No preferences text found.");
            fvm.setFeelingDescription("Custom","No custom text or image set. Please check settings =)");
        }else{
            Log.d(TAG, "Preferences text found: " + preferencesCustomText);
            fvm.setFeelingDescription("Custom", preferencesCustomText);
        }

        /*
        Here we are calling a helper method to set the UI.
        No need to pass anything as all data is in ViewModel :)
         */
        setUI();

        /*
        Listeners for toggles between view modes
         */
        feelingDescriptionTV.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });
        findViewById(R.id.rtn_to_fullscreen_button).setOnTouchListener(mDelayHideTouchListener);

        /*
        After everything is finally set up and initialized, we can create the start dialog
         */
        createStartDialog();
    }

    /**
     * Method that runs post every run/create of FeelingsActivity
     *
     * @param savedInstanceState is the input bundle for the saved instance
     */
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        delayedHide(100);
    }

    /**
     * Method that overrides back button press to prevent leaving Zenn Session
     */
    @Override
    public void onBackPressed() {
        /*
        This is to keep the user focused in their session. May consider revising if it
        negatively affects flow too much.
         */
        Toast.makeText(this, "Please finish your ZennSession™.", Toast.LENGTH_LONG).show();
    }

    /**
     * Method that runs when an option is selected
     *
     * @param item is the passed item of the menu that was selected
     * @return returns a boolean status variable
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            NavUtils.navigateUpFromSameTask(this);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Toggles the show/hide methods based on mVisible
     *
     */
    private void toggle() {
        if (mVisible) { hide(); }
        else { show(); }
    }

    /**
     * Hides the appropriate UI elements when switching view styles
     *
     */
    private void hide() {
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) { actionBar.hide(); }
        mControlsView.setVisibility(View.GONE);
        mVisible = false;
        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Shows the appropriate UI elements when switching view styles
     *
     */
    @SuppressLint("InlinedApi")
    private void show() {
        feelingDescriptionTV.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;
        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    /**
     * Hides the appropriate UI elements when switching view styles on a delay
     *
     * @param delayMillis is how many milliseconds to delay the hide by
     */
    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    /**
     * Creates the start ZennSession dialog
     *
     */
    private void createStartDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("For the next 60 seconds, breathe and follow the instructions.");
        builder.setTitle("Welcome to your ZennSession™.");
        builder.setPositiveButton("Start", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                /*
                Starts timer subscribe and fades in text.
                 */
                subscribeTimer();
                fadeInTV();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });
        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * Sets the appropriate UI elements based on the FeelingsActivityViewModel
     *
     */
    private void setUI(){
        switch(fvm.currentFeeling){
            case "Custom":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Custom"));
                //feelingCustomButton.setVisibility(View.VISIBLE);
                break;
            case "Guilt":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Guilt"));
                feelingIV.setImageResource(R.drawable.guilt);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            case "Sadness":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Sadness"));
                feelingIV.setImageResource(R.drawable.sadness);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            case "Anxiety":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Anxiety"));
                feelingIV.setImageResource(R.drawable.anxiety);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            case "Fear":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Fear"));
                feelingIV.setImageResource(R.drawable.fear);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            case "Stress":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Stress"));
                feelingIV.setImageResource(R.drawable.stress);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            case "Anger":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Anger"));
                feelingIV.setImageResource(R.drawable.anger);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            case "Neutral":
                feelingDescriptionTV.setText(fvm.getFeelingDescription("Neutral"));
                feelingIV.setImageResource(R.drawable.neutral);
                Log.d(TAG,"image set to " + fvm.currentFeeling);
                break;
            default:
                feelingDescriptionTV.setText("ERROR 420: MainFrame Destabilized");
                feelingIV.setImageResource(R.drawable.baseline_hourglass_empty_24);
                Log.d(TAG,"image set to " + "ERROR");
                Log.d(TAG, "fvm.currentFeeling = " + fvm.currentFeeling);
        }
    }

    /**
     * Updates the timer text view with the passed string
     *
     * @param value is the text to display
     */
    private void displayTimerValue(String value) {
        timerTV.setText(String.valueOf(value));
    }

    /**
     * Subscribes/starts the timer and observer
     *
     */
    private void subscribeTimer(){
        fvm.startTimer();
        fvm.getElapsedTime().observe(this,elapsedTimeObserver);
    }

    /**
     * Unsubscribes/stops the timer and observer and finishes the activity
     *
     */
    private void unsubscribeTimer(){
        fvm.stopTimer();
        fvm.getElapsedTime().removeObserver(elapsedTimeObserver);
        finish();
    }

    /**
     * Fades in the text view
     *
     */
    private void fadeInTV(){
        Animation fadeIn = AnimationUtils.loadAnimation(this,R.anim.fade_in);
        fadeIn.setDuration(3000);
        if(feelingDescriptionTV.getAlpha()==0.0f) {
            feelingDescriptionTV.setAlpha(1.0f);
            feelingDescriptionTV.startAnimation(fadeIn);
        }
    }

    /**
     * Fades out the text view
     *
     */
    private void fadeOutTV(){
        Animation fadeOut = AnimationUtils.loadAnimation(this,R.anim.fade_out);
        fadeOut.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                feelingDescriptionTV.setAlpha(0.0f);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        fadeOut.setDuration(3000);
        if(feelingDescriptionTV.getAlpha()==1.0f) {
            feelingDescriptionTV.startAnimation(fadeOut);
        }
    }

}