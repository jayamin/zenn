package edu.iastate.zenn.controller;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.NotificationCompat;
import androidx.core.app.NotificationManagerCompat;
import androidx.lifecycle.ViewModelProviders;
import androidx.preference.PreferenceManager;

import edu.iastate.zenn.R;
import edu.iastate.zenn.model.MainActivityViewModel;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Calendar;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;
import static androidx.core.app.NotificationCompat.DEFAULT_VIBRATE;

/**
 * Main activity where the user will spend their initial interaction.
 *
 * This activity is the CONTROLLER that interfaces with the data in MainActivityViewModel
 * and updates the VIEW in activity_main.xml accordingly. It is where the user can choose their
 * feeling and see the top level menu. They can also enter a ZennSession from here.
 *
 * @author Jay Amin
 * @version 0.01
 */
public class MainActivity extends AppCompatActivity {

    /**
     * Tag for custom logging
     */
    public String TAG = "CustomLog_MainActivity";

    /**
     * ViewModel
     */
    private MainActivityViewModel mvm;

    /**
     * Main floating action button
     */
    public FloatingActionButton fab;

    /**
     * Number picker with feeling choices
     */
    public NumberPicker feelingsPicker;

    /**
     * Choose image button that is dynamic
     */
    public Button chooseImgButton;

    /**
     * TextView for AI text
     */
    public TextView aiTV;

    /**
     * SharedPreferences for data persistence
     */
    public SharedPreferences sharedPreferences;

    /**
     * Method that runs every time MainActivity is created
     *
     * @param savedInstanceState is the input bundle for the saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*
        This block of code associates the ViewModel and sets the current
        feeling in case the user does an action without changing the picker
         */
        mvm = ViewModelProviders.of(this).get(MainActivityViewModel.class);
        mvm.setCurrentFeeling("Neutral");

        fab = findViewById(R.id.fab);
        feelingsPicker = findViewById(R.id.feelings_num_picker);
        chooseImgButton = findViewById(R.id.img_pick_button);
        aiTV = findViewById(R.id.ai_text_view);
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

        /*
        This block of code uses a local boolean from shared preferences to
        determine if the custom ai text should be visible
         */
        boolean customAiPref = sharedPreferences.getBoolean("ai",false);
        if(customAiPref){
            aiTV.setVisibility(View.VISIBLE);
            aiTV.setText(sharedPreferences.getString("aiText","Zen is still learning about you."));
        }else{
            aiTV.setVisibility(View.INVISIBLE);
        }

        /*
        This block of code uses a local boolean from shared preferences to
        determine if the custom button and image should be used or not
         */
        boolean customImgPref = sharedPreferences.getBoolean("image", false);
        Log.d(TAG,"customImgPref is: " + customImgPref);
        if (customImgPref){
            chooseImgButton.setVisibility(View.VISIBLE);
        }else {
            sharedPreferences.edit().putString("imageFile","").apply();
            mvm.setImageUri(null);
            chooseImgButton.setVisibility(View.INVISIBLE);
        }

        /*
        This block of code uses a local boolean from shared preferences to
        set or cancel our persistent notification
         */
        boolean customNotifPref = sharedPreferences.getBoolean("notification", false);
        boolean tempToggle = sharedPreferences.getBoolean("toggle",true);
        if (customNotifPref){
            if (tempToggle){
                setPersistentNotification();
                sharedPreferences.edit().putBoolean("toggle",false).apply();
            }
        } else{
            cancelPersistentNotification();
            sharedPreferences.edit().putBoolean("toggle",true).apply();
        }

        /*
        This block of code uses a local string from shared preferences to
        bring the saved image into the ViewModel
         */
        String tempString = sharedPreferences.getString("imageFile","");
        Log.d(TAG,"tempString = " + tempString);
        if(sharedPreferences.getString("imageFile","").equals("")){
            Log.d(TAG,"null sharedpref url returned");
        }else{
            Log.d(TAG,"result is: " + sharedPreferences.getString("imageFile","error 69"));
            String tempString2 = sharedPreferences.getString("imageFile","");
            Uri tempUri = Uri.parse(tempString2);
            mvm.setImageUri(tempUri);
        }

        /*
        Helper methods to configure the floating action button and picker.
         */
        configPicker(feelingsPicker);
        configFab(fab);
    }

    /**
     * Method that inflates the menu
     *
     * @param menu is the menu object that is passed to create the options menu
     * @return returns a boolean status variable
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    /**
     * Method that runs when an option is selected
     *
     * @param item is the passed item of the menu that was selected
     * @return returns a boolean status variable
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            startActivity(new Intent(this, SettingsActivity.class));
            return true;
        }

        if (id == R.id.action_repo) {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://git.ece.iastate.edu/jayamin/zenn"));
            startActivity(browserIntent);
            return true;
        }

        if (id == R.id.action_help) {
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO);
            emailIntent.setData(Uri.parse("mailto:jayamin@iastate.edu"));
            try {
                startActivity(emailIntent);
            } catch (Exception e) {
                Toast.makeText(this,"Error getting help =(",Toast.LENGTH_LONG).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * Method that runs on activity result from the bonus image intent
     *
     * @param requestCode is passed for consistency
     * @param data is the data passed by the intent that was the "result"
     * @param resultCode is the int determining success which is checked for
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);
        if(resultCode != RESULT_CANCELED) {
            Log.d(TAG,"Made it to onActivityResult");
            if (requestCode == 1) {
                if(resultCode == RESULT_OK && data != null){

                    /*
                    This block of text takes the resulting uri from the custom image
                    and sets it using the ViewModel and puts it in shared preferences as well.
                     */
                    Uri imageUri = data.getData();
                    Log.d(TAG, "Image URI used to be: " + mvm.getImageUri());
                    mvm.setImageUri(imageUri);
                    Log.d(TAG, "Image URI is now: " + mvm.getImageUri());
                    SharedPreferences.Editor editor = sharedPreferences.edit();
                    editor.putString("imageFile", String.valueOf(mvm.getImageUri()));
                    editor.apply();
                }

            }
        }
    }

    /**
     * Method that runs when the custom image button is pressed
     *
     * @param v is the view that is passed of the button in case customization is desired
     */
    public void chooseImgButtonPressed(View v){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Set your custom image.")
                .setItems(mvm.getCustomImgDialogChoicesArray(),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int selected) {
                                /*
                                This block of code is the bonus set image feature.
                                It was coded out to add options for additional sources such
                                as social media, direct camera, and third party apps (in the
                                future).
                                 */
                                if(selected == 0){
                                    Log.d(TAG,"Gallery option selected");
                                    Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                    startActivityForResult(pickPhoto , 1);

                                }else{
                                    Log.d(TAG,"Error in dialog click");
                                }
                            }
                        });

        AlertDialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);
        dialog.setCancelable(false);
        dialog.show();
    }

    /**
     * Method that configures the number picker on the main screen
     *
     * @param p is the numberpicker element that is passed for configuration
     */
    public void configPicker(final NumberPicker p){
        p.setMaxValue(mvm.getFeelingsArray().length-1);
        p.setMinValue(0);
        p.setDisplayedValues(mvm.getFeelingsArray());

        p.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker numberPicker, int i, int i1) {
                int pickerResult = p.getValue();
                mvm.setCurrentFeeling(mvm.getFeelingsArray()[pickerResult]);
            }
        });
    }

    /**
     * Method that configures the floating action button
     *
     * @param f is the passed floating action button to configure
     */
    public void configFab(final FloatingActionButton f){
        f.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean tempBoolean = sharedPreferences.getBoolean("image", false);

                /*
                Version 1 of AI / User Data Model for Zenn to process
                 */
                ai_V1();

                /*
                This block of code starts the FeelingsActivity / ZennSession and passes
                the custom image parameters via intent.
                 */
                Intent intent = new Intent(getBaseContext(), FeelingsActivity.class);
                intent.putExtra("CURRENT_FEELING", mvm.getCurrentFeeling());
                if(mvm.getImageUri() != null && tempBoolean){
                    intent.putExtra("CUSTOM_IMG_URI", String.valueOf(mvm.getImageUri()));
                }else{
                    intent.putExtra("CUSTOM_IMG_URI", "");
                }
                startActivity(intent);
            }
        });
    }

    /**
     * Method that sets the persistent notification
     *
     */
    private void setPersistentNotification(){
        createNotificationChannel();
        PendingIntent pendingMain = PendingIntent.getActivity(this, 0, new Intent(this, MainActivity.class), 0);
        NotificationCompat.Builder notification = new NotificationCompat.Builder(this, "420")
                .setSmallIcon(R.drawable.neutral)
                .setContentTitle("Start a ZennSession™")
                .setContentText("Start a ZennSession from anywhere!")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE)
                .setOngoing(true)
                .setContentIntent(pendingMain);
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.notify(69, notification.build());
    }

    /**
     * Method that creates the notification channel for higher Android versions
     *
     */
    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = "Persistent ZennSession starter";
            String description = "Start a ZennSession from anywhere";
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel channel = new NotificationChannel("420", name, importance);
            channel.setDescription(description);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Method that cancels the notification
     *
     */
    private void cancelPersistentNotification(){
        NotificationManagerCompat nm = NotificationManagerCompat.from(this);
        nm.cancelAll();
    }

    /**
     * Method that contains the first version of AI. Interfaces with ViewModel too :)
     *
     */
    private void ai_V1(){
        int tempMorningStat, tempAfternoonStat, tempNightStat, tempHour;
        tempMorningStat = sharedPreferences.getInt("morningStat",0);
        tempAfternoonStat = sharedPreferences.getInt("afternoonStat",0);
        tempNightStat = sharedPreferences.getInt("nightStat",0);
        tempHour = Calendar.getInstance().get(Calendar.HOUR_OF_DAY);
        Log.d(TAG, "Data PULL (curHH : mo : af : ni) = " + tempHour + " : " + tempMorningStat
                + " : " + tempAfternoonStat + " : " + tempNightStat);

        if(5<=tempHour && tempHour<12){
            tempMorningStat++;
        }else if(tempHour>12 && tempHour<18){
            tempAfternoonStat++;
        }else{
            tempNightStat++;
        }

        mvm.setMorningStat(tempMorningStat);
        mvm.setAfternoonStat(tempAfternoonStat);
        mvm.setNightStat(tempNightStat);

        sharedPreferences.edit().putInt("morningStat",mvm.getMorningStat()).apply();
        sharedPreferences.edit().putInt("afternoonStat",mvm.getAfternoonStat()).apply();
        sharedPreferences.edit().putInt("nightStat",mvm.getNightStat()).apply();
        Log.d(TAG, "Data SET (mo : af : ni) = " + mvm.getMorningStat()
                + " : " + mvm.getAfternoonStat()+ " : " + mvm.getNightStat());

        if(sharedPreferences.getBoolean("ai",false)){
            Log.d(TAG,"Received confirmation from ai settings toggle");
            Log.d(TAG,"ACTING ON DATA");
            ai_V1_Helper();
        }else{
            Log.d(TAG,"Received negative confirmation from ai settings toggle");
            Log.d(TAG,"DOING NOTHING WITH DATA");
        }
    }

    /**
     * This is the helper method for the AI that procedurally generates the AI text.
     *
     */
    private void ai_V1_Helper(){
        mvm.setProceduralBlockOne("It looks like your ");
        mvm.setProceduralBlockThree(" have been a struggle. Consider ");
        StringBuilder aiString = new StringBuilder();

        if(mvm.getMorningStat() > mvm.getAfternoonStat() && mvm.getMorningStat() > mvm.getNightStat())
        {
            //procedurally generate morning max text
            mvm.setProceduralBlockTwo("mornings");
            mvm.setProceduralBlockFour("stretching when you wake up and meditating.");
        }
        else if(mvm.getAfternoonStat() > mvm.getNightStat())
        {
            //procedurally generate afternoon max text
            mvm.setProceduralBlockTwo("afternoons");
            mvm.setProceduralBlockFour("standing up or switching positions mid-day.");
        }
        else
        {
            //procedurally generate night max text
            mvm.setProceduralBlockTwo("nights");
            mvm.setProceduralBlockFour("putting your devices away one hour before bed.");
        }

        /*
        Appending first block
         */
        aiString.append(mvm.getProceduralBlockOne());

        /*
        Appending second block
         */
        aiString.append(mvm.getProceduralBlockTwo());

        /*
        Appending third block
         */
        aiString.append(mvm.getProceduralBlockThree());

        /*
        Appending fourth block
         */
        aiString.append(mvm.getProceduralBlockFour());

        /*
        Updating TextView
         */
        Log.d(TAG,"ai text attempting to store as: " + aiString.toString());
        sharedPreferences.edit().putString("aiText",aiString.toString()).apply();
    }
}
