package edu.iastate.zenn.controller;

import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.preference.PreferenceFragmentCompat;

import edu.iastate.zenn.R;

/**
 * Settings activity that houses the settings fragment
 *
 * This activity is the CONTROLLER that interfaces with the data in root_preferences.xml
 * and updates the VIEW in settings_activity.xml accordingly. It is where the user can choose their
 * settings and see credits.
 *
 * @author Jay Amin
 * @version 0.01
 */
public class SettingsActivity extends AppCompatActivity {

    /**
     * Method that runs every time SettingsActivity is created
     *
     * @param savedInstanceState is the input bundle for the saved instance
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.settings_activity);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.settings, new SettingsFragment())
                .commit();
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /**
     * This method forces the user to use the upper back button that has more consistent behavior
     *
     */
    @Override
    public void onBackPressed()
    {
        /*
        Was seeing some bugs with standard back behavior versus app bar back.
        Didn't have enough time to step into core methods and behavior.
        This is a temporary solution until I can properly override and manage the
        lifecycles according to spec.
         */
        Toast.makeText(this, "Please use back button above.", Toast.LENGTH_LONG).show();
    }

    /**
     * Settings fragment
     *
     * This is the fragment that populates SettingsActivity
     *
     */
    public static class SettingsFragment extends PreferenceFragmentCompat {
        @Override
        public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
            setPreferencesFromResource(R.xml.root_preferences, rootKey);
        }
    }

}