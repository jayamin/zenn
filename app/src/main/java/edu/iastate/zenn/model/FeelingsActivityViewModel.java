package edu.iastate.zenn.model;

import android.os.Handler;
import android.os.Looper;
import android.os.SystemClock;
import android.util.Log;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

/**
 * FeelingsActivityViewModel that has data for FeelingsActivity
 *
 * This activity is the model for the controller FeelingsActivity.
 *
 * @author Jay Amin
 * @version 0.01
 */
public class FeelingsActivityViewModel extends ViewModel {
    /**
     * Tag for custom logging
     */
    private String TAG = "CustomLog_FeelingsActivityViewModel";

    /**
     * MutableLiveData for timer
     */
    private MutableLiveData<Long> elapsedTime = new MutableLiveData<>();

    /**
     * Current feeling
     */
    public String currentFeeling = "";

    /**
     * Initial time
     */
    private long initialTime = 0;

    /**
     * Hash map for feeling descriptions
     */
    private HashMap<String, String> feelingDescriptions = new HashMap<String,String>();

    /**
     * FeelingsActivityViewModel
     */
    public FeelingsActivityViewModel(){
        populateFeelingsHashMap();
        Timer timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                final long newValue = (SystemClock.elapsedRealtime() - initialTime) / 1000;
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        elapsedTime.setValue(newValue);
                    }
                });
            }
        }, 1000, 1000);
    }

    /**
     * Getter for elapsed time
     *
     * @return returns the livedata for elapsed time
     */
    public LiveData<Long> getElapsedTime() {
        return elapsedTime;
    }

    /**
     * Setter for elapsed time
     *
     * @param initialTime is the initial time to set
     */
    public void setInitialTime(long initialTime){
        this.initialTime = initialTime;
    }

    /**
     * Starts timer
     */
    public void startTimer() {
        Log.d(TAG,"Timer Started");
        setInitialTime(SystemClock.elapsedRealtime());
    }

    /**
     * Stops timer
     */
    public void stopTimer() {
        Log.d(TAG,"Timer Stopped");
    }

    /**
     * Getter for current feeling
     *
     * @return returns the current feeling
     */
    public String getCurrentFeeling() {
        return currentFeeling;
    }

    /**
     * Setter for current feeling
     *
     * @param currentFeeling is the feeling that needs the be set
     */
    public void setCurrentFeeling(String currentFeeling) {
        this.currentFeeling = currentFeeling;
    }

    /**
     * Getter for feeling description
     *
     * @param feeling is the feeling the description is requested for
     * @return returns the feeling description
     */
    public String getFeelingDescription(String feeling){
        return feelingDescriptions.get(feeling);
    }

    /**
     * Setter for feeling description
     *
     * @param description is description for feeling
     * @param feeling is the feeling name
     */
    public void setFeelingDescription(String feeling, String description){
        feelingDescriptions.put(feeling, description);
    }

    /**
     * Populates feelings hash map
     */
    public void populateFeelingsHashMap(){
        feelingDescriptions.put("Neutral",
                "Breathe as you think 'it's good to be here.' Smile at the little things " +
                        "and enjoy the moment.");
        feelingDescriptions.put("Anger",
                "Breathe as you think 'what made me angry?' Ask yourself if it's a small or " +
                        "big deal, and then explore more neutral compromises without anger.");
        feelingDescriptions.put("Stress",
                "Breathe as you focus on your stress. If it is unjustified, try to change your " +
                        "position or drink a glass of water. If it is justified, turn the " +
                        "stress over on itself and use it to jump-start a task that needs to get " +
                        "done.");
        feelingDescriptions.put("Fear",
                "Breathe as you think about what is causing your fear. The purpose of fear isn't " +
                        "'to make you more stressed, but rather to help you navigate potential " +
                        "danger' - no matter the context. Use it to your advantage.");
        feelingDescriptions.put("Anxiety",
                "Breathe as you think about what is around you. Research has shown anxiety " +
                        "heightens your senses. Use that to your advantage to take in the " +
                        "beautiful world around you one deep breath at a time.");
        feelingDescriptions.put("Sadness",
                "Breathe as you reflect on what made you sad. Often, it will be dissatisfaction " +
                        "with yourself or an event out of your control. It can also be a good " +
                        "thing, because it means you are passionate about something. It helps " +
                        "to step back and get some perspective.");
        feelingDescriptions.put("Guilt",
                "Breathe as you think about the source of your guilt. If it is unjustified, take a " +
                        "few deep breaths and don't internalize it. If it is justified, use it " +
                        "as a 'catalyst to make changes in your life.'");
    }
}
