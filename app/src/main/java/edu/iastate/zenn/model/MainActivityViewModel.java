package edu.iastate.zenn.model;

import android.net.Uri;
import androidx.lifecycle.ViewModel;

/**
 * MainActivityViewModel that has data for MainActivity.
 *
 * This activity is the model for the controller MainActivity.
 *
 * @author Jay Amin
 * @version 0.01
 */
public class MainActivityViewModel extends ViewModel {
    /**
     * Tag for custom logging
     */
    private String TAG = "CustomLog_MainActivityViewModel";

    /**
     * Current feeling in MainActivityViewModel
     */
    private String currentFeeling = "";

    /**
     * Feelings array for numberpicker
     */
    private String[] feelingsArray = new String[] {"Neutral","Anger", "Stress", "Fear", "Anxiety",
            "Sadness", "Guilt", "Custom"};

    /**
     * Array for custom image picker dialog
     */
    private String[] customImgDialogChoicesArray = new String[] {"Choose from gallery"};

    /**
     * Image Uri for custom image
     */
    private Uri imageUri = null;

    /**
     * ints for ai stats
     */
    private int morningStat=0,afternoonStat=0,nightStat=0;

    /**
     * Block string for ai
     */
    private String proceduralBlockOne = "[...]";

    /**
     * Block string for ai
     */
    private String proceduralBlockTwo = "[...]";

    /**
     * Block string for ai
     */
    private String proceduralBlockThree = "[...]";

    /**
     * Block string for ai
     */
    private String proceduralBlockFour = "[...]";

    /**
     * Getter for current feeling
     *
     * @return returns current feeling
     */
    public String getCurrentFeeling() {
        return currentFeeling;
    }

    /**
     * Setter for current feeling
     *
     * @param currentFeeling is the current feeling to be set
     */
    public void setCurrentFeeling(String currentFeeling) {
        this.currentFeeling = currentFeeling;
    }

    /**
     * Getter for custom image uri
     *
     * @return returns image uri
     */
    public Uri getImageUri() { return imageUri; }

    /**
     * Setter for custom image uri
     *
     * @param imageUri is the image uri to be set
     */
    public void setImageUri(Uri imageUri) {
        this.imageUri = imageUri;
    }

    /**
     * Getter for feelings array
     *
     * @return returns the feelings array
     */
    public String[] getFeelingsArray() {
        return feelingsArray;
    }

    /**
     * Setter for feelings array
     *
     * @param feelingsArray is the input feelings array to be set
     */
    public void setFeelingsArray(String[] feelingsArray) {
        this.feelingsArray = feelingsArray;
    }

    /**
     * Getter for dialog choices array
     *
     * @return returns the custom image picker choices array
     */
    public String[] getCustomImgDialogChoicesArray() {
        return customImgDialogChoicesArray;
    }

    /**
     * Getter for morning stat
     *
     * @return returns the morning stat
     */
    public int getMorningStat() {
        return morningStat;
    }

    /**
     * Setter for morning stat
     *
     * @param morningStat is the input morningStat to be set
     */
    public void setMorningStat(int morningStat) {
        this.morningStat = morningStat;
    }

    /**
     * Getter for afternoon stat
     *
     * @return returns the afternoon stat
     */
    public int getAfternoonStat() {
        return afternoonStat;
    }

    /**
     * Sets the afternoon stat
     *
     * @param afternoonStat is the input afternoonStat to be set
     */
    public void setAfternoonStat(int afternoonStat) {
        this.afternoonStat = afternoonStat;
    }

    /**
     * Getter for night stat
     *
     * @return returns the night stat
     */
    public int getNightStat() {
        return nightStat;
    }

    /**
     * Setter for night stat
     *
     * @param nightStat is the input nightStat to be set
     */
    public void setNightStat(int nightStat) {
        this.nightStat = nightStat;
    }

    /**
     * Procedural block getter
     *
     * @return returns the procedural string block
     */
    public String getProceduralBlockOne() {
        return proceduralBlockOne;
    }

    /**
     * Procedural block setter
     *
     * @param proceduralBlockOne is the procedural string block to be set
     */
    public void setProceduralBlockOne(String proceduralBlockOne) {
        this.proceduralBlockOne = proceduralBlockOne;
    }

    /**
     * Procedural block getter
     *
     * @return returns the procedural string block
     */
    public String getProceduralBlockTwo() {
        return proceduralBlockTwo;
    }

    /**
     * Procedural block setter
     *
     * @param proceduralBlockTwo is the procedural string block to be set
     */
    public void setProceduralBlockTwo(String proceduralBlockTwo) {
        this.proceduralBlockTwo = proceduralBlockTwo;
    }

    /**
     * Procedural block getter
     *
     * @return returns the procedural string block
     */
    public String getProceduralBlockThree() {
        return proceduralBlockThree;
    }

    /**
     * Procedural block setter
     *
     * @param proceduralBlockThree is the procedural string block to be set
     */
    public void setProceduralBlockThree(String proceduralBlockThree) {
        this.proceduralBlockThree = proceduralBlockThree;
    }

    /**
     * Procedural block getter
     *
     * @return returns the procedural string block
     */
    public String getProceduralBlockFour() {
        return proceduralBlockFour;
    }

    /**
     * Procedural block setter
     *
     * @param proceduralBlockFour is the procedural string block to be set
     */
    public void setProceduralBlockFour(String proceduralBlockFour) {
        this.proceduralBlockFour = proceduralBlockFour;
    }
}