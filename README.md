# ZENN

Zenn is a digitalized friend for your brain powered by AI.

## Installation

Open or import into Android Studio. Do a grade clean, build, and sync if necessary.

```
gradle --refresh-dependencies
gradle --clean
gradle --build
```

## Usage

```c
Run in your emulator of choice.
For best performance, run on a trusted device
(with USB Debugging enabled)
```

## Contributing
Please contribute and pull and let me know what you come up with!

For big ideas, open an issue or contact me.

Please follow formatting and javadoc in similar fashion.

Thanks :)

## Authors and Acknowledgement
Huge thanks to Dr. Mat and his TAs in CPRE 388 at ISU (Fall 2019).

Additional credits in app menu.

Designed by Jay Amin [ jayamin@iastate.edu ].

## License
[MIT](https://choosealicense.com/licenses/mit/)
